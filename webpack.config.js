const path = require('path');

const webpack = require('webpack');
const validate = require('webpack-validator');
const combineLoaders = require('webpack-combine-loaders');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');


const webpackConfig = {
  context: path.resolve(__dirname, 'src'),

  devServer: {
    https: false,
    host: 'localhost',
    port: '8000',

    contentBase: path.resolve(__dirname, 'src'),
    historyApiFallback: true,
    compress: true,

    hot: true,
    inline: true,

    // --progress - [assets, children, chunks, colors, errors, hash, timings, version, warnings]
    stats: {
      assets: true,
      children: true,
      chunks: false,
      colors: true,
      errors: true,
      errorDetails: true, //depends on {errors: true}
      hash: true,
      modules: false,
      publicPath: true,
      reasons: false,
      source: true, //what does this do?
      timings: true,
      version: true,
      warnings: true
    }
  },

  devtool: 'source-map',

  entry: {
    app: [
      'react-hot-loader/patch',
      './index.js'
    ]
  },
  output: {
    path: path.resolve(__dirname, 'build'),
    publicPath: 'http://localhost:8000/',
    filename: 'app.bundle.js'
  },

  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: [
          path.resolve(__dirname, 'src/components'),
          path.resolve(__dirname, 'src/Entry'),
          path.resolve(__dirname, 'src/index.js')
        ]
      },
      {
        test: /\.css$/,
        include: [
          path.resolve(__dirname, 'src/assets/styles'),
          path.resolve(__dirname, 'src/Entry'),
          path.resolve(__dirname, 'src/components')
        ],
        exclude: [
          path.resolve(__dirname, 'src/assets/styles/resets.css')
        ],
        loader: combineLoaders([
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader',
            query: {
              modules: true,
              localIdentName: '[folder]__[local]--[hash:base64:10]',
              sourceMap: true,
              importLoaders: 2,
              import: false,
              url: true
            }
          },
          {
            loader: 'resolve-url-loader',
            query: {
              sourceMap: true,
              silent: false,
              fail: true,
              keepQuery: false
            }
          },
          {
            loader: 'postcss-loader',
            query: {
              sourceMap: true
            }
          }
        ])
      },
      {
        test: /\.css$/,
        include: [
          path.resolve(__dirname, 'src/assets/styles/resets.css')
        ],
        loader: combineLoaders([
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader',
            query: {
              localIdentName: '[local]',
              sourceMap: true
            }
          }
        ])
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/,
        include: [
          path.resolve(__dirname, 'src/assets/images')
        ],
        loader: 'url-loader',
        query: {
          limit: 10000,
          name: 'assets/images/[name].[ext]'
        }
      },
      {
        test: /\.[ot]tf$/,
        include: [
          path.resolve(__dirname, 'src/assets/fonts')
        ],
        loader: 'file-loader',
        query: {
          mimetype: 'application/octet-stream',
          name: 'assets/fonts/[name].[ext]'
        }
      }
    ]
  },

  plugins: [
    new CaseSensitivePathsPlugin({
      debug: false
    }),

    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development')
      }
    }),

    new HtmlWebpackPlugin({
      template: 'index.html',
      inject: true,
      hash: true,
      cache: true,
      chunksSortMode: 'dependency',
      showErrors: true
    }),
    // Enable multi-pass compilation for enhanced performance
    // in larger projects. Good default.
    new webpack.HotModuleReplacementPlugin({
      multiStep: true
    }),
    //see possible syntax errors at the browser console instead of hmre overlay
    new webpack.NoErrorsPlugin()
  ],

  postcss: () => {
    return [
      require('postcss-smart-import')({
        root: path.resolve(__dirname, 'src'),
        path: [ 'assets', 'components'],
        skipDuplicates: true
      }),
      require('postcss-cssnext')()
    ];
  },

  resolve: {
    root: path.join(__dirname),

    alias: {
      Styles: path.resolve(__dirname, 'src/assets/styles'),
      Images: path.resolve(__dirname, 'src/assets/images')
    },

    extensions: ['', '.js', '.jsx'],
    modulesDirectories: ['node_modules', 'src']
  }
};

module.exports = validate(webpackConfig, {
  rules: {
    'no-root-files-node-modules-nameclash': true, //default
    'loader-enforce-include-or-exclude': false,
    'loader-prefer-include': false
  }
});
