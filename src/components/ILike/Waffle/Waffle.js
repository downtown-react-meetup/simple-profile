import React from 'react';

import CSS from './waffle.css';
import pup1 from 'Images/pup1.jpg';
import pup3 from 'Images/pup3.jpg';
import pup4 from 'Images/pup4.jpg';


const Waffle = () => {
  return (
    <div className={ CSS['container'] }>
      Presenting: <span className={ CSS['highlight'] }>Waffle</span>
      <section className={ CSS['images'] }>
        <img src={ pup1 } alt="pup1"/>
        <img src={ pup4 } alt="pup1"/>
        <img src={ pup3 } alt="pup1"/>
        <div className={ CSS['intheback'] }></div>
      </section>
    </div>
  );
};


export default Waffle;
