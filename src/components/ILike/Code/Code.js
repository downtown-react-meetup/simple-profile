import React from 'react';

import CSS from './code.css';


const myFavorites = ['javascript', 'css', 'html', '& node'];

const Code = () => {
  return (
    <div className={ CSS['container'] }>
      <span className={ CSS['intro'] }>I like to play with code</span>
      <div className={ CSS['things'] }>
        {
          myFavorites.map((el, i) => {
            return (
              <div className={ CSS[`index-${i}`] }>{ el }</div>
            );
          })
        }
      </div>
    </div>
  );
};


export default Code;
