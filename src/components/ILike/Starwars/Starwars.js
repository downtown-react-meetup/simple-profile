import React from 'react';

import CSS from './starwars.css';
import poe from 'Images/poe.jpeg';


const Starwars = () => {
  return (
    <div className={ CSS['container'] }>
      starwars
      <div className={ CSS['image'] }>
        <img src={ poe } alt="poe" />
      </div>
    </div>
  );
};


export default Starwars;
