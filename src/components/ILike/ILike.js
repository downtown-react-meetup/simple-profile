import React from 'react';

import CSS from './ilike.css';

import Code from './Code/Code';
import Waffle from './Waffle/Waffle';
import Starwars from './Starwars/Starwars';


const ILike = () => {
  return (
    <div className={ CSS['container'] }>
      <div className={ CSS['ilike'] }>
        <Code />
      </div>
      <div className={ CSS['ilike'] }>
        <Waffle />
      </div>
      <div className={ CSS['ilike'] }>
        <Starwars />
      </div>
    </div>
  );
};

export default ILike;
