import React from 'react';

import CSS from './intro.css';


const Intro = () => {
  return (
    <header className={ CSS['intro'] }>
      Roberto Fuentes
    </header>
  );
};


export default Intro;
