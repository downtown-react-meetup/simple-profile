import React from 'react';

import CSS from './entry.css';

import 'Styles/fonts.css';
import 'Styles/normalizer.css';
import 'Styles/resets.css';

import Intro from '../components/Intro/Intro';
import ILike from '../components/ILike/ILike';


const Entry = () => {
  return (
    <div className={ CSS['profile-page'] }>
      <Intro />
      <ILike />
    </div>
  );
};


export default Entry;
